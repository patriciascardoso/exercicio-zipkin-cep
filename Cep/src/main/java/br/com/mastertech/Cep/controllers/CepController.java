package br.com.mastertech.Cep.controllers;

import br.com.mastertech.Cep.clients.Cep;
import br.com.mastertech.Cep.services.CepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CepController {

    @Autowired
    private CepService cepService;

    @GetMapping("/cep/{cep}")
    public Cep create(@PathVariable String cep) {
        return cepService.getByCep(cep);
    }

}

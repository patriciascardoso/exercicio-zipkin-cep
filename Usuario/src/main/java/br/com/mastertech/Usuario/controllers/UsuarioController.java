package br.com.mastertech.Usuario.controllers;

import br.com.mastertech.Usuario.clients.Cep;
import br.com.mastertech.Usuario.clients.CepClient;
import br.com.mastertech.Usuario.models.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {

    @Autowired
    private CepClient cepClient;


    @GetMapping("/usuario/{name}/{cep}")
    public Usuario create(@PathVariable String name, @PathVariable String cep) {
        Usuario usuario = new Usuario();
        usuario.setNome(name);

        Cep byCep = cepClient.getCep(cep);
        usuario.setBairro(byCep.getBairro());
        usuario.setLogradouro(byCep.getLogradouro());

        return usuario;
    }
}
